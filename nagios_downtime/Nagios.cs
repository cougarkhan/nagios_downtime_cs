﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nagios_downtime
{
    class Nagios
    {
        public string hostname { get; set; }
        public int port { get; set; }
        public string username { get; set; }
        public string private_key { get; set; }
        public List<string> commands { get; set; }

        public string localhost { get; set; }
        public bool services { get; set; }
        public string duration { get; set; }
        public string author { get; set; }
        public string comment { get; set; }


        public Nagios()
        {
            string exe_path = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);
            hostname = "bis-nagios.bussops.ubc.ca";
            port = 22;
            username = "nagremote";
            private_key = exe_path + "\\test";
            commands = new List<string>();

            localhost = "BIS-FS1-PRD";
            //localhost = Environment.MachineName;
            services = false;
            author = Environment.UserName;
            duration = "900";
            comment = "Restart";
            //comment = "Restart Server by " + author + " at " + DateTime.Now.ToString();
        }

            
    }
}
