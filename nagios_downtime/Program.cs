﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Renci.SshNet;
using System.IO;
using System.Threading;
using System.Collections;

namespace nagios_downtime
{
    class Program
    {
        static void Main(string[] args)
        {
            Nagios nagios = new Nagios();
            TimeSpan epoch = DateTime.Now.Subtract(new DateTime(1970, 1, 1, 0, 0, 0, 0));
            uint origin = Convert.ToUInt32(epoch.TotalSeconds);
            string start = origin.ToString();
            string end = Convert.ToString(origin + (Convert.ToInt32(nagios.duration)));
            string command_string_1 = "/home/nagremote/nagios-downtimes/host-downtime.sh 1 " + nagios.localhost + " " + nagios.duration + " " + nagios.author + @" """ + nagios.comment + @"""";
            string command_string_2 = @"grep -A 11 -i ""hostdowntime"" /usr/local/nagios/var/status.dat | grep -A 9 -i """ + nagios.localhost + @"""";
            nagios.commands.Add(command_string_1);
            nagios.commands.Add(command_string_2);
            ConsoleColor orig_color = Console.ForegroundColor;

            try
            {
                ConnectionInfo conn_info = new ConnectionInfo(nagios.hostname, nagios.username, new PasswordAuthenticationMethod(nagios.username, ""), new PrivateKeyAuthenticationMethod(nagios.username, new PrivateKeyFile(nagios.private_key)));
                SshClient ssh_client = new SshClient(conn_info);

                ssh_client.Connect();
                foreach (string command in nagios.commands)
                {
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.WriteLine(command);
                    Console.ForegroundColor = orig_color;

                    var cmd = ssh_client.CreateCommand(command);

                    var asynch = cmd.BeginExecute(null, null);
                    var reader = new StreamReader(cmd.OutputStream);
                    while (!asynch.IsCompleted)
                    {
                        Thread.Sleep(100);
                        Hashtable entries = new Hashtable();
                        while (!reader.EndOfStream)
                        {
                            string line = reader.ReadLine().Trim();
                            char[] delimiter = { '=' };
                            string[] lines = line.Split(delimiter, StringSplitOptions.RemoveEmptyEntries);
                            if (lines.Length > 1)
                                entries.Add(lines[0], lines[1]);
                        }

                        foreach (DictionaryEntry line in entries)
                        {
                            Console.ForegroundColor = ConsoleColor.Green;
                            Console.WriteLine("{0,-15} : {1}", line.Key, line.Value);
                        }


                        //if (string.IsNullOrEmpty(result))
                        //    continue;


                    }
                    cmd.EndExecute(asynch);

                    Thread.Sleep(1000 * 10);
                }



                ssh_client.Disconnect();
            }
            catch (System.ArgumentException ae)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("There are now multiple downtimes scheduled for {0}!", nagios.localhost);
                Console.ForegroundColor = orig_color;
            }
            catch (SystemException e)
            {
                Console.WriteLine(e);
            }
            


            Console.ReadKey();
        }
    }
}
